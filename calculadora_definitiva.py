#!/usr/bin/python3

import sys #cuando llamo a algo de sys, lo hago sys.*
import decimal

#Operations definitions
def suma(A,B):
    return A+B
def resta(A,B):
    return A-B
def multiplicacion(A,B):
    return A*B
def division(A,B):
    try:
        resultado= A/B
    except decimal.DivisionByZero:
        resultado= "Error: Divison by Zero not allowed:"
    return resultado

#Check entry
#try:

#Argumentos de control
if(len(sys.argv) !=4):
    sys.exit("Usage error: $ calc.py OPERATION ARG1 ARG2")
operacion= sys.argv[1]
if operacion not in ['suma', 'resta', 'multiplicacion', 'division']:
    sys.exit("solo valen sum resta multi y div")

try:
    op1 = decimal.Decimal(sys.argv[2])
    op2 = decimal.Decimal(sys.argv[3])
except decimal.InvalidOperation:
    sys.exit("Me tienes que pasar numeros")

#Main
if operacion == "suma":
    res= suma(op1,op2)
elif operacion == "resta":
    res= resta(op1,op2)
elif operacion == "multiplicacion":
    res= multiplicacion(op1,op2)
elif operacion == "division":
    res= division(op1,op2)
print(res)
